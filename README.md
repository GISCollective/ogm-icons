Green Map Icons
=====================

This repository contains the OGM icons used on maps.

Welcome! Green Map Icons, version 3 are available for mapping and more - all to help you and your
community thrive and become a healthier and more sustainable place! Now available under the Creative
Commons rather than by copyright, you can use them according to the overview and
[licensing guidelines](https://www.greenmap.org/about/license). This includes giving credit to Green Map,
sharing alike and non commercial use on a variety of projects (with reciprocal support from income generation).

Co-designed since 1995, the Green Map Icons evolve with our understanding of sustainability. Our
thanks to everyone who took part in the development of Green Map Icons, Version 3. This set of Green Map
Icons includes 170 icons in 12 categories arranged in 3 genres. Generally, Green Map projects do not use
all of the icons on a single map.

Several resources, including the Icon font and image sets, are
available to make them easier to use. At http://GreenMap.org/icons, find these and public resources that
will be useful in workshops and project outreach.

Icons can be added to this platform, too!

More details to come.

License
--------

Green Map System, Inc. has re-licensed the formerly copyrighted Green Map Icons,
v3 for use under the Creative Commons BY-NC-SA license (+CC).
Details: https://new.greenmap.org/about/license

